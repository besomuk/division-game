var config =
{
    type: Phaser.WEBGL,
    width: 1024,
    height: 768,
    physics:
    {
        default: 'arcade',
        arcade:
        {
            gravity: { y: 200 }
        }
    },
    scene:
    {
        preload: preload,
        create: create
    }
};

var game = new Phaser.Game(config);

var score = 0;        // current score
var highScore = 0;    // high score
var gameOver = false; // game over status

var gameW = 3; // main matrix width
var gameH = 3; // main matrix height

var tileBar  = new Array(); // holds random elements
var tileBarG = new Array(); // holds random element graphics

// Two dimensional arrays that holds values and graphics in main matrix
var tableArray =
                [
                    [0,0,0],
                    [0,0,0],
                    [0,0,0]
                ];

var tableArrayG =
                [
                    [0,0,0],
                    [0,0,0],
                    [0,0,0]
                ];

var reserveTile = 0;        // reserve tile value
var reserveTileG;       // reserve tile graphics
var reserveTileCarying = false;                   // used for putting number in reserve field from number bar
var reserveTileCaryingFromReserveField = false;   // user for putting number from reserve to main matrix
var totalDivisions = 0; // total number of divisions in one pass

var gameOverGfx;
var UI_txtGameOver;
var UI_txtPlayAgain;
var UI_txtScoreValue;

var tempDragPosition =
{
    x: 0,
    y: 0
}

var gridArray = new Array(); // holds all points, their screen position and grid positions...
/* *** end of vars *** */

/* returns grid position, from screen position */
function ReturnPosition ( gridA, point )
{
    for ( var i = 0; i < gridA.length; i++)
    {
        if ( point.x == gridA[i].x && point.y == gridA[i].y )
        {
            var point = 
                {
                  a: gridA[i].a,
                  b: gridA[i].b
                };            
            return point;        
        }
    }
    return -1;    
}

/* Creates one tile. Checks what happens when that tile hits drop zone */
function CreateTile ( gameContext, tile )
{
    //console.log ( "Tile name: " + tile.name );

    var bg = gameContext.add.sprite(0, 0, 'buttonBG');
    var bgTXT = gameContext.add.bitmapText(0, 0, 'carrier_command', tile.value, 50);
    var txtContainer = gameContext.add.container ( -bgTXT.width/2, -bgTXT.height/2, [bgTXT]);
    var container = gameContext.add.container(tile.x, tile.y, [ bg, txtContainer ]);
    container.setSize(bg.width, bg.height);

    bg.setTint ( Math.random() * 0xffffff );

    container.setInteractive();
    gameContext.input.setDraggable(container);

    
    container.on('dragstart', function(pointer, localX, localY, event)
    {
        console.log ("Drag start...");
        this.setDepth(100);
    });
    


    container.on('drag', function(pointer, localX, localY, event)
    {
        if ( tile.name == 'tile3')
        {
            this.x = localX;
            this.y = localY;
        }

        if ( tile.name == 'reserve')
        {
            this.x = localX;
            this.y = localY;
            reserveTileCarying = true;
        }
    
    });

    container.on('dragend', function (pointer, gameObject, dropped)
    {    
        this.setDepth(1);
        //console.log ("! -- " + tile.name + " @ " + tempDropZoneX + ", " + tempDropZoneY );
        if ( tile.name == 'reserve')
        {
            //console.log ("ovaj je dosao iz rezerve");
            reserveTileCaryingFromReserveField = true;
        }
        
        // check if field is occupied
        //var fieldOccupied = true;
        
        console.log ( tile.name );
        if ( CheckDropOnDropZone( gameContext, this.x, this.y ) && (tile.name == 'tile3' || tile.name == 'reserve'))
        {    
            if ( tile.name == 'tile3'|| tile.name == 'reserve')
            {
                container.input.enabled = false;

                // rearrange bar array
                if ( !reserveTileCaryingFromReserveField )
                {
                    tileBarG[0].x = 333;
                    tileBarG[1].x = 466;

                    tileBar[1].name = 'tile3';
                    tileBar[1].x    = 466;

                    tileBar[0].name = 'tile2';
                    tileBar[0].x    = 333;
                }
                
                console.log ( tempDropZoneX, tempDropZoneY );
                var tp = new Object();
                tp.x = tempDropZoneX;
                tp.y = tempDropZoneY;
                var p = ReturnPosition( gridArray, tp );
                console.log (p);
                    
                                
                // If reservre field, do reserve field stuff....
                if ( tempDropZoneX == 700 && tempDropZoneY == 600 )
                {
                    //tableArray[2][2] = tileBar[2].value;
                    //tableArrayG[2][2] = tileBarG[2];
                    console.log('rezerva');
                    reserveTile = tileBar[2].value;
                    reserveTileG = tileBarG[2];
                    reserveTile.name = "reserve";
                    tile.name = "reserve";
                    container.input.enabled = true;
                }
                else // this is drop on 'regular' field
                {
                    AssigneValueToField ( p.a, p.b, reserveTileCaryingFromReserveField );
                }

                CheckH( gameContext );
                CheckV( gameContext );

                console.log ("Total divisions: " + totalDivisions );

                // Check for game over
                if ( IsGameOver() )
                {
                    console.log ("GAME OVER!");
                    CreateGameOverPanel( gameContext );
                }
                else
                {
                    totalDivisions = 0;
                    if ( reserveTileCarying)
                    {
                        reserveTileCarying = false;
                    }
                    else
                    {
                        MakeNewBarElement( gameContext );
                    }
                }
        }
        }
        else
            {
                console.log ("Ne valja drop zone, vrati ga na mesto");
                if ( tile.name == 'tile3')
                {
                    this.x = 466;
                    this.y = 600;
                }
                if ( tile.name == 'tile2')
                {
                    this.x = 333;
                    this.y = 600;
                }
                if ( tile.name == 'tile1')
                {
                    this.x = 200;
                    this.y = 600;
                }                
            }
       });
    
    
    var tempDropZoneX, tempDropZoneY; // stores temporary drop zone coords

    gameContext.input.on('drop', function (pointer, gameObject, dropZone, event)
    {
        gameObject.x = dropZone.x;
        gameObject.y = dropZone.y;
        tempDropZoneX = dropZone.x;
        tempDropZoneY = dropZone.y;
    });
    

    return container;
}

/* Checks if element is dropped on dropzone or on field that is already occupied */
function CheckDropOnDropZone( gameContext, tempDropZoneX, tempDropZoneY )
{
    console.log ( "res: " + reserveTile  );
    
    var tp = new Object();
    tp.x = tempDropZoneX;
    tp.y = tempDropZoneY;
    var p = ReturnPosition( gridArray, tp );
    
    for ( var i = 0; i < gridArray.length; i++)
    {
        if ( gridArray[i].x == tempDropZoneX && gridArray[i].y == tempDropZoneY )
        {
            if ( tableArray[gridArray[i].a][gridArray[i].b] == 0)
                return true;
        }
    }
    
    if ( tempDropZoneX == 700 && tempDropZoneY == 600 && reserveTile == 0 ) return true;
    

    return false;
}

/* Assignes value to field. Checks if number is coming from number bar, or reserve field */
function AssigneValueToField ( x, y, reserve )
{
    if ( reserve ) // if carried from reserve, assign values from reserveTile
    {
        tableArray[x][y] = reserveTile;
        tableArrayG[x][y] = reserveTileG;
        reserveTileCaryingFromReserveField = false;
        reserveTile = 0;
    }
    else // if carried from number bar, assign values of third element
    {
        tableArray[x][y] = tileBar[2].value;
        tableArrayG[x][y] = tileBarG[2];
    }
}

/* Calls CheckTableH */
function CheckH( gameContext )
{
// Check table for possible divisions horizontal
    for ( var i = 0; i < gameW; i++)
    {
        for ( var j = 0; j < gameH; j++)
        {
            CheckTableH ( gameContext, i, j);
        }
    }    
}

/* Calls CheckTableB */  
function CheckV( gameContext )
{
    // Check table for possible divisions vertical
    for ( var i = 0; i < ( gameW - 1 ); i++)
    {
        for ( var j = 0; j < gameH; j++)
        {
            CheckTableV ( gameContext, i, j);
        }
    }    
}

/* Checks entire table for possible divions */
function CheckTableH ( gameContext, r, c, placeDropped )
{
    // Check hor...
    if ( IsDiv ( tableArray[r][c], tableArray[r][c+1] ))
    {
        // console.log ("Check table");
        var tempX = tableArrayG[r][c].x;
        var tempY = tableArrayG[r][c].y;

        tableArrayG[r][c].destroy();
        tableArrayG[r][c+1].destroy();

        const t = new Tile( gameContext, config );
        t.x = tempX;
        t.y = tempY;
        t.name = 'tile3';
        t.value = tableArray[r][c] / tableArray[r][c+1];

        // calculate score
        var n2 = tableArray[r][c+1];
        totalDivisions++;
        score += n2 * totalDivisions;

        if ( score >= highScore )
            highScore = score;

        UpdateUI( gameContext );

        if ( t.value == 1 )
        {
            tableArray[r][c] = 0;
            tableArray[r][c+1] = 0;
        }
        else
        {
            // create new tile, with result of division
            var tg = CreateTile( gameContext, t );

            tableArray[r][c+1] = 0;
            tableArray[r][c] = t.value;
            tableArrayG[r][c] = tg;            
        }
        
        CheckH ( gameContext ); // aj jos jedan krug, sve dok ima deljenja
        CheckV ( gameContext );
    }
}

/* Checks entire table for possible vertical divions.... Duplicating... This should be combined with CheckTableV */
function CheckTableV ( gameContext, r, c, placeDropped )
{   
    // Check vertical...
    if ( IsDiv ( tableArray[r][c], tableArray[r+1][c] ))
    {
        var tempX = tableArrayG[r][c].x;
        var tempY = tableArrayG[r][c].y;

        tableArrayG[r][c].destroy();
        tableArrayG[r+1][c].destroy();

        const t = new Tile( gameContext, config );
        t.x = tempX;
        t.y = tempY;
        t.name = 'tile3';
        t.value = tableArray[r][c] / tableArray[r+1][c];

        // calculate score
        var n2 = tableArray[r+1][c];
        totalDivisions++;
        score += n2 * totalDivisions;

        if ( score >= highScore )
            highScore = score;

        UpdateUI( gameContext );

        if ( t.value == 1 )
        {
            tableArray[r][c] = 0;
            tableArray[r+1][c] = 0;
        }
        else
        {
            // create new tile, with result of division
            var tg = CreateTile( gameContext, t );

            tableArray[r+1][c] = 0;
            tableArray[r][c] = t.value;
            tableArrayG[r][c] = tg;            
        }
        
        CheckV ( gameContext );
        CheckH ( gameContext );
        
    }
    
}

/* Checks if game is over ( no more possible divisions, all fields are filled ) */
function IsGameOver()
{
    for ( var i = 0; i < gameW; i++ )
    {
        for ( var j = 0; j < gameH; j++ )
        {
            if ( tableArray[i][j] == 0 )
            {
                gameOver = true;
                return false;
            }
        }
    }    
    return true;
    
}

/* Makes new element in random bar */
function MakeNewBarElement ( gameContext )
{
    // make new element..
    const t = new Tile( gameContext, config );
    t.x = 200;
    t.y = 600;
    t.name = 'tile1';
    t.value = GetRandomNumber();

    var tg = CreateTile( gameContext, t );

    // reposition elements in array
    tileBar.splice ( 0, 0, t);
    tileBar.length = 3;
    tileBarG.splice ( 0, 0, tg);
    tileBarG.length = 3;

    //console.log ( tileBar );
    console.log ( tableArray );
    //console.log ( "Reserve tile: " + reserveTile );
}

/* Creates random numbers bar */
function CreateNumberBar( gameContext )
{
    // this should be configurable...
    var initX = 200;
    var initY = 600;
    var tileWidth = 128;
    var margin = 5;
    var newX = initX;
    var newY = initY;

    for ( var i = 0; i < 3; i++ )
    {
        const t = new Tile( gameContext, config );
        t.x = newX;
        t.y = newY;
        t.name = 'tile' + (i+1);
        t.value = GetRandomNumber();

        var tg = CreateTile( gameContext, t );
        newX += tileWidth + margin;
        tileBar.push ( t );
        tileBarG.push ( tg );
    }

    console.log ( tileBar );
    console.log ( tableArray );

}

/* Creates game table / grid with dropzones */
function CreateEmptyTable ( gameContext )
{
    // this should be configurable...
    var counterX = 0;    // how many elements on x
    var counterY = 0;    // how many elements on y
    var initX = 200;     // initial x position
    var initY = 110;     // initial y position
    var newX = initX;
    var newY = initY;
    var gap = 5;         // space between tile
    var tileWidth = 128; // width of tile

    // add drop zones. Those are also our table presentation...
    while ( counterY < gameW)
    {
        while ( counterX < gameH )
        {
            var grid = 
                {
                    x: 0, 
                    y: 0,
                    a: 0,
                    b: 0
                };
            grid.x = newX;
            grid.y = newY;
            grid.a = counterY;
            grid.b = counterX;
            gridArray.push ( grid );            
            //zone = gameContext.add.zone(newX-64, newY-64).setRectangleDropZone(128);
            zone = gameContext.add.zone(newX, newY).setCircleDropZone(64);
            var graphics = gameContext.add.graphics();
            graphics.lineStyle(2, 0xffff00);
            //graphics.strokeRect(zone.x, zone.y, 128, 128);
            graphics.strokeCircle(zone.x, zone.y, zone.input.hitArea.radius);
            
            graphics.setDepth(0);

            newX += tileWidth + gap;
            counterX ++;
        }
        newY += tileWidth + gap;
        counterY ++;
        newX = initX;
        counterX = 0;
    }

    // also create spare number field
    zone = gameContext.add.zone(700, 600).setCircleDropZone(64);
    var graphics = gameContext.add.graphics();
    graphics.lineStyle(2, 0xffff00);
    graphics.strokeCircle(zone.x, zone.y, zone.input.hitArea.radius);
    //graphics.setDepth(-2000);
}

/* Restarts scene */
function RestartScene ( gameContext )
{
    // Clear matrix
    for ( var i = 0; i < gameW; i ++ )
    {
        for ( var j = 0; j < gameH; j ++ )
        {
            tableArray[i][j] = 0;
            tableArrayG[i][j].destroy();
        }
    }

    // Clear number bar
    for ( var i = 0; i < 3; i ++ )
    {
        tileBar[i].value = 0;
        tileBarG[i].destroy();
    }

    // If set, clear reserve field
    if ( reserveTileG )
    {
        reserveTile = 0;
        reserveTileG.destroy();
    }

    tileBar     = [];
    tileBarG    = [];
    score = 0;

    UpdateUI(gameContext);

    HideGameOverPanel(gameContext);
    CreateNumberBar(gameContext);
}

/* Creates UI elements */
function CreateUI ( gameContext )
{
    UI_txtScoreLabel = gameContext.add.bitmapText(780, 80, 'gothic', 'SCORE', 30).setOrigin(0.5);
    UI_txtScoreValue = gameContext.add.bitmapText(780, 80+35, 'gothic', score, 30).setOrigin(0.5);
    UI_txtHighScoreLabel = gameContext.add.bitmapText(780, 170, 'gothic', 'HIGH SCORE', 30).setOrigin(0.5);
    UI_txtHighScoreValue = gameContext.add.bitmapText(780, 170+35, 'gothic', highScore, 30).setOrigin(0.5);
}

/* Updates UI */
function UpdateUI ( gameContext )
{
    UI_txtScoreValue.setText(score);
    UI_txtHighScoreValue.setText(highScore);
}

/* Creates Game Over panel */
function CreateGameOverPanel ( gameContext )
{
    gameOverGfx = gameContext.add.graphics();
    gameOverGfx.lineStyle(2, 0x000000, 1);
    gameOverGfx.fillStyle(0x339933, 1);
    gameOverGfx.fillRoundedRect(80, 240, 870, 250, 15);
    UI_txtGameOver  = gameContext.add.bitmapText(512, 300, 'gothic', 'GAME OVER', 80).setOrigin(0.5);
    UI_txtPlayAgain = gameContext.add.bitmapText(512, 410, 'gothic', 'Play again ( y/n )?', 80).setOrigin(0.5);
    
    gameOverGfx.setDepth ( 200 );
    UI_txtGameOver.setDepth ( 201 );
    UI_txtPlayAgain.setDepth ( 201 );
}

/* Hides / clears Game Over panel */
function HideGameOverPanel ( gameContext )
{
    gameOverGfx.destroy();
    UI_txtGameOver.destroy();
    UI_txtPlayAgain.destroy();
}

/******************************** Phasers... **********************************/
function preload ()
{
    this.load.bitmapFont('carrier_command', 'assets/fonts/bitmapFonts/carrier_command.png', 'assets/fonts/bitmapFonts/carrier_command.xml');
    this.load.bitmapFont('gothic', 'assets/fonts/bitmapFonts/gothic.png', 'assets/fonts/bitmapFonts/gothic.xml');
    this.load.image('buttonBG', 'assets/buttons/button.png');
}

function create ()
{
    // creates yellow field around 'main' number
    var graphics = this.add.graphics();
    graphics.lineStyle(2, 0xffff00, 1);
    graphics.strokeRoundedRect(400, 534, 132, 132, 0);

    // prepare and draw game elements
    CreateEmptyTable( this );
    CreateNumberBar ( this );
    CreateUI ( this );

    // stores context for future use...
    var t = this;

    // Check keyboard events
    this.input.keyboard.on('keydown', function (event, a = t )
    {
        if ( event.keyCode === Phaser.Input.Keyboard.KeyCodes.Y && gameOver )
        {
            console.log ("Game over, restarting scene...");
            RestartScene( a );
        }
    });
    
    console.log ( gridArray );
}
