/* Returns random number */
function GetRandomNumber( lowLimit = 2, highLimit = 23)
{
    var number = Math.floor(Math.random() * highLimit) + lowLimit;
    return number;
}

/* Check if two numbers are divisible */
function IsDiv ( n1, n2 )
{
    if ( n1 == 0 || n2 == 0 )
        return false;

    if ( n1 % n2 == 0 )
        return true;
    else
        return false;
}

/* debug - reset all array values to default value 0 */
function resetArray ( array, value = 0 )
{
    for ( var i = 0; i < gameW; i++)
    {
        for ( var j = 0; j < gameH; j++)
        {
            array[i,j] = value;
        }
    }
}

/* debug -outputs array to console */
function showArray ( array )
{
    var str = "";

    for ( var i = 0; i < gameW; i++)
    {
        for ( var j = 0; j < gameH; j++)
        {
            str += array[i][j] + " ";
        }
        str += "\n";
    }
    console.log ( str );
    console.log ( "-" );
}
