**Installation**

Put it on web server, start with index.html

**Description and rules**

- This is 'division' game, where player earn points by placing numbers no main gridin such manner that two adjacent numbers are divisible. For example, if number on top left field is 24 and player puts number 12 right or bellow it, division will occur and result of 2 will be placed instead number 24. If result is divisible by other adjacent number, division will occur again, and player will get extra points. 

- Division will occur in horizontal right direction, and vertical down.

- Result of 1 is removed from table

- There is 'reservre' field on right side of game board, so player can save number 'for later'